// This file is part of OpenLifter, simple Powerlifting meet software.
// Copyright (C) 2019 The OpenPowerlifting Project.
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

import React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { FormattedMessage } from "react-intl";
import FormGroup from "react-bootstrap/FormGroup";

import NumberInput from "../common/NumberInput";
import YesNoButton from "../common/YesNoButton";

import { getString } from "../../logic/strings";
import { updateMeet } from "../../actions/meetSetupActions";

import { Language, Validation } from "../../types/dataTypes";
import { GlobalState } from "../../types/stateTypes";

interface StateProps {
  language: Language;
  attemptHandinTimeout: number;
  enableAttemptHandinTimer: boolean;
}

interface DispatchProps {
  setAttemptHandinTimeout: (seconds: number) => void;
  setEnableAttemptHandinTimer: (bool: boolean) => void;
}

interface OwnProps {
  // Used by the MeetSetup component to cause component updates.
  onChange: () => void;
}

type Props = StateProps & DispatchProps & OwnProps;

interface InternalState {
  initialEnabled: boolean;
  initialTimeout: number;
}

class AttemptHandinTimer extends React.Component<Props, InternalState> {
  constructor(props: Props) {
    super(props);

    this.state = {
      initialEnabled: this.props.enableAttemptHandinTimer,
      initialTimeout: this.props.attemptHandinTimeout,
    };
  }

  validateAttemptHandinTimeout = (n: number): Validation => {
    if (!Number.isInteger(n) || n <= 0) {
      return "error";
    }
    return "success";
  };

  handleToggleChange = (bool: boolean): void => {
    this.props.onChange();
    this.props.setEnableAttemptHandinTimer(bool);
  };

  render() {
    const language = this.props.language;
    const stringNo = getString("common.response-no", language);
    const stringYes = getString("common.response-yes", language);

    const timeoutInput = (
      <NumberInput
        step={5}
        initialValue={this.state.initialTimeout}
        label={
          <FormattedMessage
            id="meet-setup.attempt-handin-timeout"
            defaultMessage="Seconds before automatically setting the next attempt"
          />
        }
        validate={this.validateAttemptHandinTimeout}
        onChange={this.props.setAttemptHandinTimeout}
      />
    );

    return (
      <FormGroup>
        <YesNoButton
          label={
            <FormattedMessage
              id="meet-setup.enable-attempt-handin-timer"
              defaultMessage="Enable attempt hand-in timer"
            />
          }
          value={this.state.initialEnabled}
          setValue={this.handleToggleChange}
          yes={stringYes}
          no={stringNo}
        />

        {this.props.enableAttemptHandinTimer ? timeoutInput : null}
      </FormGroup>
    );
  }
}

const mapStateToProps = (state: GlobalState): StateProps => ({
  language: state.language,
  attemptHandinTimeout: state.meet.attemptHandinTimeout,
  enableAttemptHandinTimer: state.meet.enableAttemptHandinTimer,
});

const mapDispatchToProps = (dispatch: Dispatch): DispatchProps => {
  return {
    setAttemptHandinTimeout: (seconds) => dispatch(updateMeet({ attemptHandinTimeout: seconds })),
    setEnableAttemptHandinTimer: (bool) => dispatch(updateMeet({ enableAttemptHandinTimer: bool })),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(AttemptHandinTimer);
